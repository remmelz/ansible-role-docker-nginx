Ansible Role Docker Nginx
============================

Nginx, stylized as NGINX, nginx or NginX, is a web server that can also 
be used as a reverse proxy, load balancer, mail proxy and HTTP cache.

* https://nginx.org/en/


Requirements
------------

Tested with Debian GNU/Linux (Buster) with docker.


Role Variables
--------------

default variables:

```
project:        'prx_rvproxy'
index:          '1'
network:        'net1'
snakeoil_certs:  True 
letsenc_staging: False
lensenc_email:   'joe@example.com'
certbot_args:    ''
```

Dependencies
------------

None


License
-------

See license.md

